(function ($, Drupal) {

  Drupal.behaviors.amoebaSelectWithButton = {
    attach: function (context) {
      $(Drupal.amoebaSelectWithButton.commonParentSelector + ' select', context)
        .once('amoebaSelectWithButton')
        .on('change', Drupal.amoebaSelectWithButton.onChange);
    }
  };

  Drupal.amoebaSelectWithButton = Drupal.amoebaSelectWithButton || {};

  Drupal.amoebaSelectWithButton.commonParentSelector = '.amoeba-component-select-with-button';

  Drupal.amoebaSelectWithButton.onChange = function () {
    $(this)
      .parents(Drupal.amoebaSelectWithButton.commonParentSelector)
      .find('input[type="submit"], button')
      .trigger('mousedown');
  };

})(jQuery, Drupal);
