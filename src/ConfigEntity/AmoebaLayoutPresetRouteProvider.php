<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

class AmoebaLayoutPresetRouteProvider extends AdminHtmlRouteProvider {

}
