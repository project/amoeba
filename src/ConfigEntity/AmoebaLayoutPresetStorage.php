<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

class AmoebaLayoutPresetStorage extends ConfigEntityStorage implements AmoebaLayoutPresetStorageInterface {

}
