<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface      create(array $values = [])
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface      createFromStorageRecord(array $values)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface      updateFromStorageRecord(\Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface $entity, array $values)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface|null load($id)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface|null loadUnchanged($id)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface|null loadOverrideFree($id)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface|null loadRevision($id)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface[]    loadByProperties(array $values = [])
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface[]    loadMultiple(array $ids = NULL)
 * @method \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface[]    loadMultipleOverrideFree(array $ids = NULL)
 * @method int  save(\Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface $entity)
 * @method void restore(\Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface $entity)
 */
interface AmoebaLayoutPresetStorageInterface extends ConfigEntityStorageInterface {

}
