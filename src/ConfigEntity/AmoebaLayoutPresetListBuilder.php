<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\amoeba\Utils;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class AmoebaLayoutPresetListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'num_of_regions' => $this->t('Regions'),
      'label' => $this->t('Label'),
      'description' => $this->t('Description'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface $entity */
    $numOf = Utils::numberOfItems($entity->get('children'));
    $row = [
      'num_of_regions' => [
        'data' => $numOf['region'],
        'class' => ['content-numeric'],
      ],
      'label' => [
        'data' => Html::escape($entity->label()),
      ],
      'description' => [
        'data' => Html::escape($entity->get('description')),
      ],
    ];

    return $row + parent::buildRow($entity);
  }

}
