<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\amoeba\Utils;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

class AmoebaLayoutPresetForm extends EntityForm {

  /**
   * @var \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $storage =& $form_state->getStorage();
    $storage += ['values' => []];
    $storage['values'] += [
      'children' => $this->entity->get('children') ?: [],
    ];

    if (empty($storage['values']['children'])) {
      $storage['values']['children']['r1'] = Utils::createChild(1, 'region');
    }

    $storage['values']['num_of'] = Utils::numberOfItems($storage['values']['children']);

    $entityType = $this->entity->getEntityType();
    $keys = $entityType->getKeys();

    $form[$keys['label']] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label(),
      '#description' => $this->t(
        'The human-readable name of this %label. This name must be unique.',
        [
          '%label' => $entityType->getSingularLabel(),
        ]
      ),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form[$keys['id']] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine-readable name'),
      '#description' => $this->t(
        'A unique machine-readable name for this content type. It must only contain lowercase letters, numbers, and underscores.'
      ),
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [$entityType->getClass(), 'load'],
        'source' => [$keys['label']],
      ],
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Administrative description'),
    ];

    $form['num_of'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Number of'),
      'wrapper' => [
        '#type' => 'number',
        '#title' => $this->t('wrappers'),
        '#required' => TRUE,
        '#default_value' => $storage['values']['num_of']['wrapper'],
        '#min' => 0,
        '#max' => 48,
        '#step' => 1,
      ],
      'region' => [
        '#type' => 'number',
        '#title' => $this->t('regions'),
        '#required' => TRUE,
        '#default_value' => $storage['values']['num_of']['region'],
        '#min' => 1,
        '#max' => 16,
        '#step' => 1,
      ],
      'btn_update_num_of' => [
        '#type' => 'submit',
        '#value' => $this->t('Update'),
        '#name' => 'btn_update_num_of',
        '#parents' => ['btn_update_num_of'],
        '#submit' => ['::submitFromUpdateNumOf'],
        '#limit_validation_errors' => [
          ['num_of'],
        ],
      ],
    ];

    $form['children'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Children'),
      '#open' => TRUE,
    ];

    foreach ($storage['values']['children'] as $id => $child) {
      $child += ['id' => $id];
      $form['children'][$child['id']] = $this->elementChild($storage['values']['children'], $child);
    }

    return $form;
  }

  public function submitFromUpdateNumOf(array $form, FormStateInterface $formState) {
    $formState->setRebuild();
    $storage =& $formState->getStorage();

    $new = $formState->getValue('num_of');
    $old = $storage['values']['num_of'];
    $storage['values']['num_of'] = $new;

    foreach (['wrapper', 'region'] as $type) {
      for ($i = $old[$type] + 1; $i <= $new[$type] ; $i++) {
        $child = Utils::createChild($i, $type);
        $storage['values']['children'][$child['id']] = $child;
      }

      for ($i = $new[$type] + 1; $i <= $old[$type] ; $i++) {
        unset($storage['values']['children'][Utils::childId($i, $type)]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saveResult = parent::save($form, $form_state);
    $this->saveSetMessage();

    $route_name = 'entity.' . $this->entity->getEntityTypeId() . '.collection';
    $form_state->setRedirect($route_name);

    return $saveResult;
  }

  protected function elementChild(array $children, array $child): array {
    return [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $child['label'],
      '#open' => TRUE,
      '#weight' => $child['weight'],
      'parent' => [
        '#type' => 'select',
        '#required' => FALSE,
        '#title' => $this->t('Parent'),
        '#default_value' => $child['parent'],
        '#empty_option' => $this->t('- Select -'),
        '#empty_value' => '',
        '#options' => $this->parentOptions($children, $child),
      ],
      'weight' => [
        '#type' => 'number',
        '#title' => $this->t('Weight'),
        '#default_value' => $child['weight'],
      ],
      'label' => [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Label'),
        '#default_value' => $child['label'],
      ],
      'outer' => $this->elementTag($child['outer']),
    ];
  }

  protected function parentOptions(array $children, array $child): array{
    $options = [];
    foreach ($children as $id => $c) {
      $c += ['id' => $id];
      if (Utils::isRegion($c['id']) || $child['id'] === $c['id']) {
        continue;
      }

      $options[$c['id']] = $c['label'];
    }

    return $options;
  }

  protected function elementTag(array $values): array {
    return [
      '#type' => 'details',
      '#title' => $this->t('Outer'),
      '#tree' => TRUE,
      '#open' => TRUE,
      'tag' => [
        'name' => [
          '#type' => 'select',
          '#required' => FALSE,
          '#title' => $this->t('HTML tag'),
          '#empty_option' => $this->t('- None -'),
          '#empty_value' => '',
          '#options' => $this->tagOptions(),
          '#default_value' => $values['tag']['name'],
        ],
        'attributes' => [
          '#type' => 'fieldset',
          '#tree' => TRUE,
          '#title' => $this->t('Attributes'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          'class' => [
            '#tree' => TRUE,
            'value' => [
              '#type' => 'textfield',
              '#title' => $this->t('Class'),
              '#default_value' => $values['tag']['attributes']['class']['value'],
            ],
            'replace_tokens' => [
              '#type' => 'checkbox',
              '#title' => $this->t('Replace tokens'),
              '#default_value' => $values['tag']['attributes']['class']['replace_tokens'],
            ],
          ],
        ],
      ],
    ];
  }

  protected function tagOptions(): array {
    return [
      (string) $this->t('Neutral') => [
        'div' => 'div',
        'span' => 'span',
      ],
      (string) $this->t('Semantic') => [
        'article' => 'article',
        'aside' => 'aside',
        'dialog' => 'dialog',
        'footer' => 'footer',
        'header' => 'header',
        'main' => 'main',
        'nav' => 'nav',
        'section' => 'section',
      ],
    ];
  }

  /**
   * Place a status message after save.
   */
  protected function saveSetMessage() {
    $message = NULL;
    $args = [
      '%label' => $this->entity->label(),
      '@type' => $this->entity->getEntityType()->getLabel(),
    ];
    switch ($this->getOperation()) {
      case 'add':
        $message = $this->t('"%label" @type has been created', $args);
        break;

      case 'edit':
        $message = $this->t('"%label" @type has been updated', $args);
        break;

    }

    if ($message) {
      $this->messenger()->addStatus($message);
    }
  }

}
