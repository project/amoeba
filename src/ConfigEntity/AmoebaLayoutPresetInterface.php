<?php

declare(strict_types = 1);

namespace Drupal\amoeba\ConfigEntity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface AmoebaLayoutPresetInterface extends ConfigEntityInterface {

}
