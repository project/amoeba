<?php

declare(strict_types = 1);

namespace Drupal\amoeba;

interface AmoebaChildrenProcessorInterface {

  /**
   * @return $this
   */
  public function preprocess(array &$variables);

}
