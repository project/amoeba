<?php

declare(strict_types = 1);

namespace Drupal\amoeba;

class Utils {

  public static function numberOfItems(array $children): array {
    $items = [
      'wrapper' => 0,
      'region' => 0,
    ];
    foreach ($children as $id => $child) {
      $id = $child['id'] ?? $id;
      $type = static::isRegion($id) ? 'region' : 'wrapper';
      $items[$type]++;
    }

    return $items;
  }

  public static function isRegion(string $identifier): bool {
    return (bool) preg_match('/^r\d+$/', $identifier);
  }

  public static function childId(int $i, string $type): string {
    return mb_substr($type, 0, 1) . $i;
  }

  public static function createChildren(int $numOfRegions): array {
    $children = [];

    for ($i = 0; $i <= $numOfRegions; $i++) {
      $child = static::createChild($i, 'region');
      $children[$child['id']] = $child;
    }

    return $children;
  }

  public static function createChild(int $i, string $type): array {
    $id = static::childId($i, $type);

    return [
      'parent' => '',
      'weight' => $i,
      'id' => $id,
      'label' => $id,
      'outer' => [
        'tag' => [
          'name' => 'div',
          'attributes' => [
            'class' => [
              'value' => "{$type} {$type}-{$id}",
              'replace_tokens' => TRUE,
            ],
          ],
        ],
      ],
      'children' => [],
    ];
  }

}
