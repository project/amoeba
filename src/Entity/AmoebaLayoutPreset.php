<?php

declare(strict_types = 1);

namespace Drupal\amoeba\Entity;

use Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * @\Drupal\Core\Entity\Annotation\ConfigEntityType(
 *   id = "amoeba_layout_preset",
 *   label = @\Drupal\Core\Annotation\Translation("Amoeba layout preset"),
 *   label_singular = @\Drupal\Core\Annotation\Translation("Amoeba layout preset"),
 *   label_plural = @\Drupal\Core\Annotation\Translation("Amoeba layout presets"),
 *   label_collection = @\Drupal\Core\Annotation\Translation("List of Amoeba layout presets"),
 *   label_count = @\Drupal\Core\Annotation\PluralTranslation(
 *     singular = "@count Amoeba layout preset",
 *     plural = "@count Amoeba layout presets"
 *   ),
 *   admin_permission = "amoeba.layout_preset.admin",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   handlers = {
 *     "storage" = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetStorage",
 *     "list_builder" = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetListBuilder",
 *     "access" = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetForm",
 *       "add"     = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetForm",
 *       "edit"    = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetForm",
 *       "delete"  = "Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetDeleteConfirmForm"
 *     }
 *   },
 *   links = {
 *     "add-form"    = "/admin/structure/amoeba_layout_preset/add",
 *     "edit-form"   = "/admin/structure/amoeba_layout_preset/manage/{amoeba_layout_preset}",
 *     "delete-form" = "/admin/structure/amoeba_layout_preset/manage/{amoeba_layout_preset}/delete",
 *     "collection"  = "/admin/structure/amoeba_layout_preset"
 *   },
 *   config_prefix = "layout_preset",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "children"
 *   }
 * )
 */
class AmoebaLayoutPreset extends ConfigEntityBase implements AmoebaLayoutPresetInterface {

}
