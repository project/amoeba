<?php

declare(strict_types = 1);

namespace Drupal\amoeba\Plugin\Layout;

use Drupal\amoeba\Utils;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @\Drupal\Core\Layout\Annotation\Layout(
 *   id = "amoeba",
 *   deriver = "\Drupal\amoeba\AmoebaLayoutDeriver"
 * )
 */
class AmoebaLayout extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\amoeba\ConfigEntity\AmoebaLayoutPresetStorageInterface
   */
  protected $presetStorage;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface
   */
  protected $presetEntityType;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $etm = $container->get('entity_type.manager');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $etm->getStorage('amoeba_layout_preset'),
      $etm->getDefinition('amoeba_layout_preset')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigEntityStorageInterface $presetStorage,
    ConfigEntityTypeInterface $presetEntityType
  ) {
    $this->presetStorage = $presetStorage;
    $this->presetEntityType = $presetEntityType;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration['preset'] = '';

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['preset'] = [
      '#type' => 'select',
      '#title' => $this->t('Preset'),
      '#required' => FALSE,
      '#empty_option' => $this->t('- Choice -'),
      '#default_value' => $this->configuration['preset'],
      '#options' => $this->presetOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['preset'] = $form_state->getValue('preset');
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    $presetId = $this->configuration['preset'] ?? '';
    if ($presetId) {
      $provider = $this->presetEntityType->getProvider();
      $configPrefix = $this->presetEntityType->getConfigPrefix();
      $dependencies['config'][] = "$provider.$configPrefix.$presetId";
    }

    return $dependencies;
  }

  /**
   * @return string[]|\Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  protected function presetOptions(): array {
    $numOfRegions = count($this->getPluginDefinition()->getRegions());
    $options = [];
    foreach ($this->presetStorage->loadMultiple() as $preset) {
      $numOf = Utils::numberOfItems($preset->get('children'));
      if ($numOf['region'] === $numOfRegions) {
        $options[$preset->id()] = $preset->label();
      }
    }

    return $options;
  }

}
