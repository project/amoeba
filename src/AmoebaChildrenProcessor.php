<?php

declare(strict_types = 1);

namespace Drupal\amoeba;

use Drupal\Core\Template\Attribute;

class AmoebaChildrenProcessor implements AmoebaChildrenProcessorInterface {

  /**
   * @var array
   */
  protected $variables;

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables) {
    $this->variables =& $variables;

    $this
      ->preprocessChildId()
      ->preprocessTags()
      ->preprocessTree()
      ->preprocessRoots()
      ->preprocessSort();

    return $this;
  }

  /**
   * @return $this
   */
  protected function preprocessChildId() {
    foreach ($this->variables['children'] as $id => &$child) {
      $child += ['id' => $id];
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function preprocessTags() {
    foreach ($this->variables['children'] as $id => &$child) {
      if ($child['outer']['tag']['name']) {
        $child['outer']['tag']['attributes'] = $this->preprocessChildrenAttributes($child['outer']['tag']['attributes']);
      }
    }

    return $this;
  }

  protected function preprocessChildrenAttributes(array $values): Attribute {
    $attributes = new Attribute();
    foreach ($values as $name => $attribute) {
      // @todo Token replacement.
      if ($name === 'class' && is_string($attribute['value'])) {
        $attribute['value'] = $attribute['value'] === '' ?
          []
          : preg_split('/\s+/', trim($attribute['value']));
      }
      $attributes->setAttribute($name, $attribute['value']);
    }

    return $attributes;
  }

  /**
   * @return $this
   */
  protected function preprocessTree() {
    foreach ($this->variables['children'] as $id => &$child) {
      if (empty($child['parent'])) {
        continue;
      }

      $this->variables['children'][$child['parent']]['children'][] = $child['id'];
    }


    return $this;
  }

  protected function preprocessSort() {
    $comparer = $this->getChildComparer($this->variables['children']);
    uasort($this->variables['roots'], $comparer);
    foreach ($this->variables['children'] as &$child) {
      if (!empty($child['children'])) {
        uasort($child['children'], $comparer);
      }
    }

    return $this;
  }

  protected function preprocessRoots() {
    $this->variables['roots'] = [];
    foreach ($this->variables['children'] as $child) {
      if (empty($child['parent'])) {
        $this->variables['roots'][] = $child['id'];
      }
    }

    return $this;
  }

  protected function getChildComparer(array $children): callable {
    return function (string $a, string $b) use ($children): int {
      $result = $children[$a]['weight'] <=> $children[$b]['weight'];
      if ($result) {
        return $result;
      }

      return $children[$a]['id'] <=> $children[$b]['id'];
    };
  }

}
